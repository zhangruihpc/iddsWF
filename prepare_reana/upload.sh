version=v02
rucio upload --rse BNL-OSG2_SCRATCHDISK /eos/user/z/zhangr/REANA/input/mc15_13TeV.123456.cap_recast_demo_signal_one.root --name mc15_13TeV.123456.cap_recast_demo_signal_one_${version}.root
if [[ -v RUCIO_ACCOUNT ]]; then
    rucio add-dataset user.${RUCIO_ACCOUNT}.reana.${version}.dataset
    rucio attach user.${RUCIO_ACCOUNT}:user.${RUCIO_ACCOUNT}.reana.${version}.dataset user.zhangr:mc15_13TeV.123456.cap_recast_demo_signal_one_${version}.root
fi
unset version
