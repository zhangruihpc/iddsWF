# Git Sparse Checkout
In case you only want to checkout a single folder, eg `post_fastcalogan`, you could use sparse checkout from git
```
 git clone ssh://git@gitlab.cern.ch:7999/zhangruihpc/iddsWF.git  --no-checkout iddsWF
 cd iddsWF
 git sparse-checkout set .gitignore post_fastcalogan
 cat .git/info/sparse-checkout
 git checkout master
```
