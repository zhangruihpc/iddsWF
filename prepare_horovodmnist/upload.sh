version=v02
rucio upload --rse BNL-OSG2_SCRATCHDISK /afs/cern.ch/user/z/zhangr/wis/HPO/Horovod/input_mnist/mnist_horovod_fashion_mnist.tar --name mnist_horovod_fashion_mnist_${version}.tar
if [[ -v RUCIO_ACCOUNT ]]; then
    rucio add-dataset user.${RUCIO_ACCOUNT}.horovodmnist.${version}.dataset
    rucio attach user.${RUCIO_ACCOUNT}:user.${RUCIO_ACCOUNT}.horovodmnist.${version}.dataset user.zhangr:mnist_horovod_fashion_mnist_${version}.tar
fi
unset version
