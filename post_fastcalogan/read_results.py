#!/usr/bin/env python
# Rui Zhang 6.2021
# rui.zhang@cern.ch

import json
from glob import glob
from os import path
import pandas as pd
import copy

result='/afs/cern.ch/user/z/zhangr/wis/HPO/FastCaloGan/output/v2/user.zhangr.*/tarball_PandaJob_*/HPO_Sim_*/best_epoch/chi2/epoch_best_*.txt'

results = glob(result)

rows_list = []
rows_list_with_path = []

for ifile in results:
    file_name = path.basename(ifile)
    idds_name = path.dirname(ifile) + '/../../input.json'

    with open(idds_name, 'r') as f:
        seg, data = tuple(json.load(f))
    
    pid, eta_min, eta_max = tuple(file_name.split('.')[0].split('_')[3:])
    data['pid'], data['eta_min'], data['eta_max'] = int(pid), float(eta_min)/100, float(eta_max)/100
        
    with open(ifile, 'r') as f:
        epoch, chi2 = tuple(f.read().strip('\n').split(' '))
        data['epoch'], data['chi2'] = int(epoch), float(chi2)

    rows_list.append(data)
    data_with_path = copy.deepcopy(data)
    data_with_path['path'] = ifile
    rows_list_with_path.append(data_with_path)

df = pd.DataFrame(rows_list).sort_values(by=['chi2']).reset_index(drop=True)
df_path = pd.DataFrame(rows_list_with_path).sort_values(by=['chi2']).reset_index(drop=True)

df.to_csv('results.csv', index=False)
df_path.to_csv('results_path.csv', index=False)
