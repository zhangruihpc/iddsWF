code=~/work/HPO/FastCaloGAN/plotting
if [[ $CONDA_DEFAULT_ENV != 'hpo' ]]; then
    source ~/work/HPO/iddsWF/setup.sh
fi

cd $code

output=`\ls -d ~/wis/HPO/FastCaloGan/output/v2/user.zhangr.FCGv2*/tarball_PandaJob_*/HPO_*`
#output=`\ls -d ~/wis/HPO/FastCaloGan/output/v2/user.zhangr.FCGv2DESY_5*/tarball_PandaJob_*/HPO_Sim_normE_MaxE_High12_OPT_E_NoextrapWeightFix/photons/checkpoints_eta_5_10/../../`

for input in $output; do
    inside_input=`\ls -d $input/*s`
    index=`basename $inside_input`
    checkpoint=`\ls -d $inside_input/checkpoints_*`
    eta_min=`echo $checkpoint | rev | cut -d '/' -f 1 | rev | cut -d '_' -f 3`
    eta_max=`echo $checkpoint | rev | cut -d '/' -f 1 | rev | cut -d '_' -f 4`
    if [[ $index == *"photons"* ]]; then
        pid=22
    elif [[ $index == *'electrons'* ]]; then
        pid=11
    elif [[ $index == *'pions'* ]]; then
        pid=211
    else
        continue
    fi
    echo TF_CPP_MIN_LOG_LEVEL=2   python selectBestEpoch_Energy.py -v /eos/atlas/atlascerngroupdisk/proj-simul/VoxalisationOutputs/test_binning_2021_moreBinsL1_absphiMod_abseta_PerEventFromG4HitsEnergy_phiMod_optimisedAlphaSim  -s True -p $pid -e1 $eta_min -e2 $eta_max -emin 100000 -emax 500000 -step 10000 -idg $input
    TF_CPP_MIN_LOG_LEVEL=2   python selectBestEpoch_Energy.py -v /eos/atlas/atlascerngroupdisk/proj-simul/VoxalisationOutputs/test_binning_2021_moreBinsL1_absphiMod_abseta_PerEventFromG4HitsEnergy_phiMod_optimisedAlphaSim -s True -p $pid -e1 $eta_min -e2 $eta_max -emin 100000 -emax 500000 -step 10000 -idg $input
done

cd -
