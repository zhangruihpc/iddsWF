output=~/wis/HPO/FastCaloGan/output/v2/$1
if [ -z "$1" ]; then
    echo "No argument supplied, select from:"
    ls -1 $output 1/2/3
    return
fi

i=$2
if [[ -z "$2" ]]; then
    echo "Which step? 1/2/3"
fi

if [[ $i == 1 ]]; then
    #all_tgz=`find $output/*log.tgz -type f -size +100M`
    all_tgz=`find $output/*log.tgz -type f -size +50k`
    for input in $all_tgz; do
        tar xzf $input -C $output && \
        folder=`tar tzf $input -C $output | head -1 | cut -f1 -d"/" ` && \
        echo $folder && \
        ls $output/$folder &
    done
fi


if [[ $i == 2 ]]; then
    all_tgz=`\ls -d $output/tarball_PandaJob_*`
    for input in $all_tgz; do
        for tar in `ls $input/*_*_*.tgz_*`; do
            echo tar xf $tar -C $input
            tar xf $tar -C $input && \
            ls $input/metrics.tgz_* &
        done
    done
fi

if [[ $i == 3 ]]; then
    all_tgz=`\ls -d $output/tarball_PandaJob_*`
    for input in $all_tgz; do
        for tar in `ls $input/metrics.tgz_*`; do
            echo tar xf $tar -C $input
            tar xf $tar -C $input && \
            ls -f $input/HPO_* 
        done
    done
fi

