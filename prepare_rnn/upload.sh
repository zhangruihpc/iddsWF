version=v02
rucio upload --rse BNL-OSG2_SCRATCHDISK /afs/cern.ch/user/z/zhangr/wis/HPO/RNN/input/trt_sharded_weighted_1M5K.tar.gz --name trt_sharded_weighted_1M5K.${version}.tar.gz
if [[ -v RUCIO_ACCOUNT ]]; then
    rucio add-dataset user.${RUCIO_ACCOUNT}.rnn.${version}.dataset
    rucio attach user.${RUCIO_ACCOUNT}:user.${RUCIO_ACCOUNT}.rnn.${version}.dataset user.zhangr:trt_sharded_weighted_1M5K.${version}.tar.gz
fi
unset version
