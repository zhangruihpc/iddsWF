#!/usr/bin/env python
# Rui Zhang 2.2021
# rui.zhang@cern.ch

from __future__ import print_function
import sys
import os
import tarfile
import glob
import subprocess
import pdb

command_only = True

def execute(cmd):
    """
    Executes a command in a subprocess. Returns a tuple
    of (exitcode, out, err), where out is the string output
    from stdout and err is the string output from stderr when
    executing the command.

    :param cmd: Command string to execute
    """

    process = subprocess.Popen(cmd,
                               shell=True,
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    result = process.communicate()
    (out, err) = result
    exitcode = process.returncode

    return exitcode, out.decode(), err.decode()

def compress(output_file="archive.tar.gz", items=[]):
    """compress dirs.

    KWArgs
    ------
    output_file : str, default ="archive.tar.gz"
    items : list
        list of dirs/items relative to root dir

    """
    print('Compress to tarball: {0}'.format(output_file))
    with tarfile.open(output_file, "w:gz") as tar:
        for i, item in enumerate(items):
            if os.path.basename(os.path.dirname(item)) in ['photons', 'electrons', 'pions']:
                arcname = '/'.join(item.split('/')[-3:])
            else:
                arcname = '/'.join([os.path.basename(os.path.dirname(item)), os.path.basename(item)])
            tar.add(item, arcname=arcname)
            print('File: {0}/{1}, {2} -> {3}'.format(i, len(items), item, arcname))

def tar_name(file_name, version):
    tar_full_name = file_name.split('/')[-1].split('.')[0].split('_')
    print(tar_full_name)
    tar_full_name.pop(-1)
    tar_full_name.pop(1)
    tar_full_name = '_'.join(tar_full_name)
    tar_full_name = '/tmp/zhangr/' + tar_full_name + '.' + version + '.tar'
    return tar_full_name

def register_dataset(dataset_name, command_only = command_only):
    # 2. Register a dataset for it
    cmd = 'rucio add-dataset ' + dataset_name
    print(cmd)
    if not command_only:
        try:
            exitcode, out, err = execute(cmd)
            print(out, err)
        except:
            pass

def upload_csv(dataset_name, tar_full_name, command_only = command_only):
    # 1. upload csv file individually to rucio:
    tar_file_name = tar_full_name.split('/')[-1]
    cmd = 'rucio upload --rse BNL-OSG2_SCRATCHDISK {0} --name {1}'.format(tar_full_name, tar_file_name)
    print(cmd)
    if not command_only:
        exitcode, out, err = execute(cmd)
        print(out, err)

    # 3. Attach files to the dataset name (note that the file name has user.${RUCIO_ACCOUNT})
    cmd = 'rucio attach ' + dataset_name + ' user.{0}:'.format(os.environ['RUCIO_ACCOUNT']) + tar_file_name
    print(cmd)
    if not command_only:
        exitcode, out, err = execute(cmd)
        print(out, err)

def run(pid, eta, version, clean=False):
    particles = {'11': 'electrons', '22': 'photons', '211': 'pions' }
    files = glob.glob('/afs/cern.ch/user/z/zhangr/wis/HPO/FastCaloGan/input/input_v2/csvFiles/pid{0}_E*_eta_{1}_{2}_voxalisation.csv'.format(pid, eta[0], eta[1]))
    files += glob.glob('/afs/cern.ch/user/z/zhangr/wis/HPO/FastCaloGan/input/input_v2/rootFiles/pid{0}_E*_eta_{1}_{2}.root'.format(pid, eta[0], eta[1]))
    # for plotting
    files += glob.glob('/afs/cern.ch/user/z/zhangr/wis/HPO/FastCaloGan/input/input_v2/rootFiles/{0}/pid{1}_E*_eta_{2}_{3}*.root'.format(particles[pid], pid, eta[0], eta[1]))
    print(files[0])
    
    tar_full_name = tar_name(files[0], version)
    if clean and os.path.exists(tar_full_name):
        os.remove(tar_full_name)
    
    compress(tar_full_name, files)
    upload_csv(dataset_name, tar_full_name, command_only)

    
    if clean and os.path.exists(tar_full_name):
        os.remove(tar_full_name)

def upload_seed(command_only = command_only, clean=False):
    # 4. Upload and Attach seed
    seed_folders = glob.glob('/afs/cern.ch/user/z/zhangr/wis/HPO/FastCaloGan/input/input_v2/Baseline_normE_MaxE_*12_OPT_E_NoextrapWeightFix', recursive=False)
    import os
    for seed_folder in seed_folders:
        if seed_folder.endswith('/'):
            seed_folder = seed_folder[:-1]

        files = glob.glob('{0}/*'.format(seed_folder))
        tar_full_name = '/tmp/zhangr/{0}.tar'.format(os.path.basename(seed_folder))
        tar_file_name = '{0}.{1}.tar'.format(os.path.basename(seed_folder), version)

        if clean and os.path.exists(tar_full_name):
            os.remove(tar_full_name)
        compress(tar_full_name, files)
        if not command_only:
            exitcode, out, err = execute(cmd)
            print(out, err)

        cmd = 'rucio upload --rse BNL-OSG2_SCRATCHDISK {0} --name {1}'.format(tar_full_name, tar_file_name)
        print(cmd)
        if not command_only:
            exitcode, out, err = execute(cmd)
            print(out, err)

        cmd = 'rucio attach ' + dataset_name + ' user.{0}:'.format(os.environ['RUCIO_ACCOUNT']) + tar_file_name
        print(cmd)
        if not command_only:
            exitcode, out, err = execute(cmd)
            print(out, err)

def upload_binning(command_only = command_only):
    # 5. Upload and Attach user.zhangr:binning.xml
    tar_full_name = '/afs/cern.ch/user/z/zhangr/wis/HPO/FastCaloGan/input/input_v2/binning.xml'
    tar_file_name = 'binning.{0}.xml'.format(version)
    cmd = 'rucio upload --rse BNL-OSG2_SCRATCHDISK {0} --name {1}'.format(tar_full_name, tar_file_name)
    print(cmd)
    if not command_only:
        exitcode, out, err = execute(cmd)
        print(out, err)

    cmd = 'rucio attach ' + dataset_name + ' user.{0}:'.format(os.environ['RUCIO_ACCOUNT']) + tar_file_name
    print(cmd)
    if not command_only:
        exitcode, out, err = execute(cmd)
        print(out, err)

def run_all():
    for pid in pids:
        for eta in eta_ranges:
            # the csv files for these slices are unavailable 15/06/2021
            if pid == '11' and (eta == (200, 205) or eta == (220, 225)):
                print('skip pid =', pid, 'eta =', eta)
                continue
            run(pid, eta, version)

    upload_seed(command_only = command_only)
    upload_binning(command_only = command_only)

def run_one(tar_name):
    # pid22_eta_0_5.v01.tar
    base = tar_name.split('.')[0].split('_')
    eta = (base[2], base[3])
    pid = base[0][3:]
    run(pid, eta, version, clean=False)

version = 'v06'

dataset_name = 'user.{0}:user.{0}.fastcalogan.{1}.dataset'.format(os.environ['RUCIO_ACCOUNT'], version)
register_dataset(dataset_name, command_only)

# actually no plotting root file available for 11; so make no sense to run 11
pids = ['22'] #, '11', '22', '211']
eta_ranges = [
        (0, 5),
        (5, 10),
        (15, 20),
        (20, 25),
        (80, 85),
        (100, 105),
        (120, 125),
        (200, 205),
        (220, 225),
    ]

run_all()
#run_one('pid22_eta_0_5.{0}.tar'.format(version))

print('rucio list-files '+dataset_name)
