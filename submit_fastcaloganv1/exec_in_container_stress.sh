FILE1=$1
echo FILE1 $FILE1
particle=`echo $FILE1 | cut -d '_' -f 1`
eta_min=`echo $FILE1 | cut -d '_' -f 2`
eta_max=`echo $FILE1 | cut -d '_' -f 3`

echo "ls"
ls
echo "---------==========----------"
echo "cat input_ds"
cat input_ds.json
echo "---------==========----------"
echo "cat input"
cat input.json
echo "---------==========----------"
echo "untar input"
tar -xzf pid*.tar
echo "---------==========----------"

echo python /ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/train_conditional_wgangp.py -i dataset -ip $particle -emin $eta_min -emax $eta_max
#time python /ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/train_conditional_wgangp.py -i ./ -ip $particle -emin $eta_min -emax $eta_max --epochs 1000000
time python /ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/train_conditional_wgangp.py -i ./ -ip $particle -emin $eta_min -emax $eta_max --epochs 100
echo "Finish training"

echo "ls"
ls
echo "---------==========----------"
echo "tar -czf metrics.tgz output"
cp input.json output/
tar -czf metrics.tgz output
echo "ls"
ls
echo "---------==========----------"
