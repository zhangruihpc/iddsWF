#!/bin/bash

current_dir=$(pwd)

# These variables are used in the payload
export payload_dir=/ATLASMLHPO/payload/DLGNN4Tracking/
export TRKXOUTPUTDIR=${current_dir}"/output"
export TRKXINPUTDIR=${current_dir}"/train_10evts"

tar xvfz submit_horovod_dataset10evts.tar
pip list|grep torch

# Convert idds point to payload format
python /ATLASMLHPO/payload/DLGNN4Tracking/convert_input.py

# Run training
python /ATLASMLHPO/payload/DLGNN4Tracking/train.py ngpu=1
#python /ATLASMLHPO/payload/DLGNN4Tracking/train.py ngpu=1 distributed

# Cleanup (may not needed)
rm -fr /__pycache__/
