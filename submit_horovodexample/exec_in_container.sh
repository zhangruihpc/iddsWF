#!/bin/bash

tar xvf $DIST_DIR/mnist_horovod_fashion_mnist_v02.tar --directory $DIST_DIR
ls
# Run training
horovodrun -np 1 python train_mnist_horovod.py --fraction 0.1 --epochs 2 --work-dir $DIST_DIR

