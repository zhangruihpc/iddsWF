#!/bin/bash

current_dir=$(pwd)

# These variables are used in the payload
export payload_dir=/ATLASMLHPO/payload/DLGNN4Tracking/
export TRKXOUTPUTDIR=${current_dir}"/output"
export TRKXINPUTDIR=${current_dir}"/train_10evts"

if [[ ! -d "train_10evts" ]]; then
    curl -sSL https://cernbox.cern.ch/index.php/s/PKXAwYTfTM5gb6V/download | tar -xzvf -;
else
    echo found train_10evts, skip downloading
fi
#tar xvfz submit_horovod_dataset10evts.tar


if [[ ! -f "input.json" ]]; then
    echo emulate input.json with '{"lr": 0.001, "hidden": 256}'
    echo '{"lr": 0.001, "hidden": 256}' > input.json
else
    echo input.json exist
fi

# Convert idds point to payload format
python /ATLASMLHPO/payload/DLGNN4Tracking/convert_input.py

# Run training
python /ATLASMLHPO/payload/DLGNN4Tracking/train.py ngpu=1

# Cleanup (may not needed)
rm -fr /__pycache__/
