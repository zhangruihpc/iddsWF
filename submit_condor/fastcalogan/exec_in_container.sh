FILE1=electrons_0_5
echo FILE1 $FILE1
particle=`echo $FILE1 | cut -d '_' -f 1`
eta_min=`echo $FILE1 | cut -d '_' -f 2`
eta_max=`echo $FILE1 | cut -d '_' -f 3`
cp /ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/input.json .
mv dataset/*csv .

echo python /ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/train_conditional_wgangp.py -i dataset -ip $particle -emin $eta_min -emax $eta_max
python /ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/train_conditional_wgangp.py -i ./ -ip $particle -emin $eta_min -emax $eta_max --epochs 1000000
echo "Finish training"
