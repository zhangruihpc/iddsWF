prun --alrb \
    --containerImage docker://chnzhangrui/evaluationcontainer:latest  \
    --exec  "pwd; ls; /ATLASMLHPO/docker/ppc/runCaloImageDNN.sh" \
    --noBuild \
    --outputs output.json \
    --site ANALY_ORNL_Summit_GPU2 \
    --outDS user.zhangr.`uuidgen` \
    --nCore 8


prun --alrb --containerImage docker://alpine  \
    --exec "echo AAAAAA==== > output.dat" \
    --noBuild \
    --outputs output.dat \
    --site ANALY_ORNL_Summit_GPU2 \
    --outDS user.zhangr.summit.`uuidgen` \
    --nCore 8

#prun --containerImage docker://busybox --alrb --exec "echo 'Hello World\!' > output.json" --tmpDir /tmp --outDS user.zhangr.summit$(date +%Y%m%d%H%M%S) --noBuild --site ANALY_ORNL_Summit_GPU2 --nCore 8

phpo --loadJson config_dev.json --site  ANALY_ORNL_Summit_GPU2 --outDS user.$USER.summit.`uuidgen`  --nParallelEvaluation 1 --maxPoints 3 --searchSpaceFile search_space.json
