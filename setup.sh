#!/bin/bash

if [[ $HOSTNAME == "lxplus"* ]] || [[ $HOSTNAME == "pcuw"* ]]; then
    __conda_setup="$('/eos/user/z/zhangr/Public/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "/eos/user/z/zhangr/Public/anaconda3/etc/profile.d/conda.sh" ]; then
            . "/eos/user/z/zhangr/Public/anaconda3/etc/profile.d/conda.sh"
        else
            export PATH="/eos/user/z/zhangr/Public/anaconda3/bin:$PATH"
        fi
    fi
    unset __conda_setup
else
    __conda_setup="$('/Users/zhangrui/opt/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "/Users/zhangrui/opt/anaconda3/etc/profile.d/conda.sh" ]; then
            . "/Users/zhangrui/opt/anaconda3/etc/profile.d/conda.sh"
        else
            export PATH="/Users/zhangrui/opt/anaconda3/bin:$PATH"
        fi
    fi
    unset __conda_setup
fi

conda activate roopy38
which python
python --version

#setupATLAS
#echo "voms-proxy-init -voms atlas"
#voms-proxy-init -voms atlas
export IDDS_CONFIG=/afs/cern.ch/user/z/zhangr/work/HPO/iddsWF/idds.cfg
