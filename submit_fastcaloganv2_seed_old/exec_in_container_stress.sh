export FCGRefactored='false'
echo FCGRefactored $FCGRefactored
FILE1=$1
echo FILE1 $FILE1
particle=`echo $FILE1 | cut -d '_' -f 1`
eta_min=`echo $FILE1 | cut -d '_' -f 2`
eta_max=`echo $FILE1 | cut -d '_' -f 3`

source /ATLASMLHPO/ROOT/root_install/bin/thisroot.sh

echo "ls"
ls
echo "---------==========----------"
echo "cat input"
cat input.json
echo "---------==========----------"
echo "untar input"
tar -xvf pid*${eta_min}_${eta_max}*.tar
ls
echo "---------==========----------"

#input_folder='test_binning_2021_moreBinsL1_absphiMod_abseta_PerEventFromG4HitsEnergy_phiMod_optimisedAlphaSim'
input_folder='./'
output_folder='Baseline_Sim_normE_MaxE_High12_OPT_E_NoextrapWeightFix'

ln -s ../binning*.xml $input_folder/binning.xml
echo python /ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/train_productionV2_wgangp.py -i $input_folder -ip $particle -emin $eta_min -emax $eta_max -e 0 -odg $output_folder --epoch 1000000 --seed
time python /ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/train_productionV2_wgangp.py -i $input_folder -ip $particle -emin $eta_min -emax $eta_max -e 0 -odg $output_folder --epoch 1000000 --seed


######## Select best epochs #########
#PYTHONPATH=/ATLASMLHPO/payload/FastCaloGAN/models/tf/WGAN-GP/:$PYTHONPATH
#echo $particle
#if [[ $particle == *"photons"* ]]; then
#    pid=22
#elif [[ $particle == *'electrons'* ]]; then
#    pid=11
#elif [[ $particle == *'pions'* ]]; then
#    pid=211
#fi
#
#echo "---------==========----------"
#echo TF_CPP_MIN_LOG_LEVEL=2 python /HPO/plotting/selectBestEpoch_Energy.py -v $input_folder -s True -p $pid -e1 $eta_min -e2 $eta_max -emin 100000 -emax 500000 -step 10000 -idg $output_folder
#TF_CPP_MIN_LOG_LEVEL=2 python /ATLASMLHPO/payload/FastCaloGAN/plotting/selectBestEpoch_Energy.py -v $input_folder -s True -p $pid -e1 $eta_min -e2 $eta_max -emin 100000 -emax 500000 -step 10000 -idg $output_folder
#
#echo "---------==========----------"
#echo rm -fr $output_folder/$particle/checkpoints_*/model_*
#rm -fr $output_folder/$particle/checkpoints_*/model_*
######## Select best epochs done #########

echo "Finish training"
cp input.json $output_folder
echo "tar -czf metrics.tgz $output_folder"
tar -czf metrics.tgz $output_folder

echo "---------==========----------"
echo "after training and plotting tree ."
tree .
echo "---------==========----------"
